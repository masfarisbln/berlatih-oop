<!DOCTYPE html>
<html>
<body>

<?php
class Animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";
    
    public function __construct($string) {
        $this->name = $string;
    }

    public function get_name() {
        return "Name : " . $this->name . "<br>";
    }

    public function get_legs() {
        return "legs : " . $this->legs . "<br>";
    }

    public function get_cold_blooded() {
        return "cold blooded : " . $this->cold_blooded . "<br>";
    }
}
?>
 
</body>
</html>
<!DOCTYPE html>
<html>
<body>

<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo $sheep->get_name();
echo $sheep->get_legs();
echo $sheep->get_cold_blooded() . "<br>";

$kodok = new Frog("buduk");
echo $kodok->get_name();
echo $kodok->get_legs();
echo $kodok->get_cold_blooded();
echo "Jump : " . $kodok->jump() . "<br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->get_name();
echo $sungokong->get_legs();
echo $sungokong->get_cold_blooded();
echo "Yell : " . $sungokong->yell() . "<br>";
?>
 
</body>
</html>